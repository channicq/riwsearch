package search.queries;

import java.util.List;
import search.compare.WordVector;
import search.indexes.DB;
import search.indexes.Tokenizer;

/**
 *
 * @author clement
 */
public class BIRQuery extends Query{

    public BIRQuery(String s) {
        super(s);
    }

    @Override
    public double score(int documentid, DB database) {
        List<String> words = Tokenizer.toWords(this.query);
        double score = 0;
        for(String word : words){
        	if(database.getIndex().get(documentid).containsKey(word)){
        		score +=  Math.log(database.getIndex().size()/database.getReverse().get(word).size());
        	}
        }
        return score;
        
    }
    
}
