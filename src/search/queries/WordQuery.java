package search.queries;

import java.util.Map;
import search.indexes.DB;

/**
 *
 * @author clement
 */
public class WordQuery extends Query{

    public WordQuery(String s) {
        super(s);
    }

    @Override
    public double score(int documentid, DB database) {
        Map<String, Double> words = database.getIndex().get(documentid);
        if(words.containsKey(this.query))
            return words.get(this.query);
        else 
            return 0;
    }
    
}
