package search.queries;

import java.util.List;
import search.compare.WordVector;
import search.indexes.DB;
import search.indexes.Tokenizer;

/**
 *
 * @author clement
 */
public class VectorialQuery extends Query{

    public VectorialQuery(String s) {
        super(s);
    }

    @Override
    public double score(int documentid, DB database) {
        List<String> words = Tokenizer.toWords(this.query);
        WordVector vector = new WordVector(words);
        
        return vector.product(database.getIndex().get(documentid));
    }
    
}
