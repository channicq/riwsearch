package search.queries.binary;

import java.util.Map;

/**
 *
 * @author clement
 */
public abstract class Term {
    static final String NOT = "NOT";
    static final String OR = "OR";
    static final String AND = "AND";
    static final String POPEN = "(";
    static final String PCLOSE = ")";
    
    public abstract boolean execute(Map<String, Double> index) throws IllegalExpressionException;
}
