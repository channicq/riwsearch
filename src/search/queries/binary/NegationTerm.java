package search.queries.binary;

import java.util.Map;

/**
 *
 * @author clement
 */
public class NegationTerm extends Term{

    Term t;
    
    public NegationTerm(String term) throws IllegalExpressionException {
        this.t = TermFactory.parse(term);
    }

    @Override
    public boolean execute(Map<String, Double> index) throws IllegalExpressionException {
        return !t.execute(index);
    }
    
}
