/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search.queries.binary;

import java.util.Map;

/**
 *
 * @author clement
 */
public class WordTerm extends Term {

    private final String word;

    WordTerm(String in) {
        this.word = in.trim().toLowerCase();
    }

    @Override
    public boolean execute(Map<String, Double> index) {
        return index.containsKey(this.word);
    }

}
