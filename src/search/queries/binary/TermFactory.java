/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search.queries.binary;

/**
 *
 * @author clement
 */
public class TermFactory {
    public static Term parse(String in) throws IllegalExpressionException{
        String[] words = in.trim().split(" ");
        
        //Capturing word
        if(words.length == 1){
            //Has to be a single word
            return new WordTerm(in);
        }
        
        //Capturing (...
        if(words[0].equals(Term.POPEN)){
            int level = 0;
            int index = 1;
            String fragment = "";
            while((words[index].equals(Term.PCLOSE) == false) || level > 0){
                fragment += words[index] + " ";
                if(words[index].equals(Term.POPEN)){
                    level++;
                }
                if(words[index].equals(Term.PCLOSE)){
                    level--;
                }
                index++;
                if(index == words.length){
                    throw new IllegalExpressionException("Missing symbol "+Term.PCLOSE+" in "+in);
                }
            }
            //Capturing (...)
            if(index == words.length -1){
                return parse(fragment.trim());
            }
            //Capturing (...) AND/OR ...
            index++;
            String operation = words[index];
            String remaining = "";
            for(int i = index + 1; i < words.length; i++) {
                remaining += words[i] + " ";
            }
            return new DoubleTerm(fragment.trim(), operation, remaining.trim());
        }
        
        //Capturing ... AND/OR ...
        String start = "";
        String operation = null;
        String end = "";
        for (String word : words) {
            if(operation != null)
                end += word + " ";
            else if(word.equals(Term.OR))
                operation = Term.OR;
            else if(word.equals(Term.AND))
                operation = Term.AND;
            else
                start += word + " ";
        }
        if(operation != null){
            return new DoubleTerm(start.trim(), operation, end.trim());
        }
        
        //Capturing expression on the form NOT ...
        if(words[0].equals(Term.NOT)){
            return new NegationTerm(in.trim().substring(3).trim());
        }
        
        throw new IllegalExpressionException("Parser could not make sense of " + in);
    }
    
    
}
