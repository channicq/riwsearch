package search.queries.binary;

import java.util.Map;

/**
 *
 * @author clement
 */
public class DoubleTerm extends Term {

    private final Term t1;
    private final Term t2;
    private final String operation;

    DoubleTerm(String t1, String operation, String t2) throws IllegalExpressionException {
        this.operation = operation;
        this.t1 = TermFactory.parse(t1);
        this.t2 = TermFactory.parse(t2);
    }

    @Override
    public boolean execute(Map<String, Double> index) throws IllegalExpressionException {
        if (operation.equals(OR)) {
            return t1.execute(index) || t2.execute(index);
        }
        if (operation.equals(AND)) {
            return t1.execute(index) && t2.execute(index);
        }
        throw new IllegalExpressionException("Illegal operation : " + operation);
    }

}
