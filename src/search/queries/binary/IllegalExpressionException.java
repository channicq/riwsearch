package search.queries.binary;

import search.queries.QueryException;


/**
 *
 * @author clement
 */
public class IllegalExpressionException extends QueryException{

    IllegalExpressionException(String string) {
        super(string);
    }
}
