/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search.queries;

import search.indexes.DB;

/**
 *
 * @author clement
 */
public abstract class Query {
    String query;
    
    public Query(String s){
        this.query = s;
    }
    
    public abstract double score(int documentid, DB database) throws QueryException;
}
