/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search.queries;

import search.indexes.DB;
import search.queries.binary.IllegalExpressionException;
import search.queries.binary.Term;
import search.queries.binary.TermFactory;

/**
 *
 * @author clement
 */
public class BinaryQuery extends Query{
    private final Term compiledQuery;

    public BinaryQuery(String s) throws IllegalExpressionException {
        super(s);
        this.compiledQuery = TermFactory.parse(s);
    }

    @Override
    public double score(int documentid, DB database) throws IllegalExpressionException{
        if(this.compiledQuery.execute(database.getIndex().get(documentid)))
            return 1;
        return 0;
    }
    
}
