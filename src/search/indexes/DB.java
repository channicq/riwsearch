package search.indexes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import search.model.Document;
import search.model.Result;
import search.queries.Query;
import search.queries.QueryException;

public class DB {

    Map<Integer, Document> documents;
    Map<Integer, Map<String, Double>> index;
    Map<String, Map<Integer, Map<String, Double>>> ponderedIndexes;
    Map<String, Map<Integer, Integer>> reverse;
    private String ponderationMode;
    private Tokenizer tokenizer;
    private Double minimalScore;
    private int maxResults;

    public DB() {
        this.documents = new HashMap<>();
        this.index = new HashMap<>();
        this.reverse = new HashMap<>();
        this.ponderedIndexes = new HashMap<>();
        this.ponderationMode = Ponderator.BASE;
        this.tokenizer = new Tokenizer();
        this.minimalScore = 0.00001;
        this.maxResults = 10;
    }

    /**
     * Sets the minimal score to be considered as a valid result.
     * @param score 
     */
    public void setMinimalScore(double score) {
        this.minimalScore = score;
    }
    
    /**
     * Sets the max number of results to be returned by execute, 0 for unlimited
     * @param number 
     */
    public void setMaxResults(int number) {
        this.maxResults = number;
    }

    /**
     * Loads stop_words file
     *
     * @param path
     */
    public void loadStopWords(String path) {
        if (index.size() > 0) {
            System.out.println("Warning : loading stopWords after loading documents.");
        }
        try {
            tokenizer.load(path);
        } catch (IOException ex) {
            System.out.println("Error : could not load stop words in file : " + path);
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Sets to ponderation method to the required one and computes the index if
     * needed.
     *
     * @param str
     */
    public void setPonderation(String str) {
        this.ponderationMode = str;
        System.out.println("Switching ponderation to : " + str);
        if (ponderedIndexes.containsKey(str) == false && (false == str.equals(Ponderator.BASE))) {
            try {
                ponderedIndexes.put(str, Ponderator.generate(str, index, reverse));
                System.out.println("Switched ponderation to : " + str);
            } catch (PonderationException ex) {
                System.out.println(ex.getMessage());
                this.ponderationMode = Ponderator.BASE;
            }
        }
    }

    /**
     * Adds a document to the in-memory database
     *
     * @param doc
     */
    public void addDocument(Document doc) {
        //Resets the ponderation method if needed
        if (ponderationMode.equals(Ponderator.BASE) == false) {
            System.out.println("WARNING : Adding a new document, switching back to " + Ponderator.BASE + " ponderation.");
            ponderationMode = Ponderator.BASE;
            this.ponderedIndexes = new HashMap<>();
        }
        documents.put(doc.getId(), doc);
        for (String word : doc.getWords()) {
            if (tokenizer.isStopWord(word) == false) {
                addWord(doc.getId(), word);
            }
        }
    }

    /**
     * Returns the document of specified id
     *
     * @param docid
     * @return
     */
    public Document getDocument(int docid) {
        return documents.get(docid);
    }

    /**
     * Executes a Query and returns an ordered Result list
     *
     * @param q Query to be executed
     * @return List of Result
     */
    public List<Result> execute(Query q) throws QueryException {
        List<Result> results = new ArrayList<>();
        for (Map.Entry<Integer, Document> entry : documents.entrySet()) {
            double score = q.score(entry.getKey(), this);
            if (score >= this.minimalScore) {
                results.add(new Result(entry.getValue(), score));
            }
        }

        //sort the results list
        Collections.sort(results, new Comparator<Result>() {
            @Override
            public int compare(Result r1, Result r2) {
                if (r2.getScore() < r1.getScore()) {
                    return -1;
                } else if (r2.getScore().equals(r1.getScore())) {
                    return 0;
                }
                return 1;
            }
        });

        //limits the result list
        if (this.maxResults > 0) {
            return results.subList(0, maxResults);
        }

        return results;
    }

    /**
     * Returns the index
     *
     * @return
     */
    public Map<Integer, Map<String, Double>> getIndex() {
        if (this.ponderationMode.equals(Ponderator.BASE)) {
            return index;
        } else {
            return ponderedIndexes.get(ponderationMode);
        }
    }

    /**
     * Returns the reversed index
     *
     * @return
     */
    public Map<String, Map<Integer, Integer>> getReverse() {
        return reverse;
    }

    /**
     * Adds a word to the index and to the reverse index
     *
     * @param id Id of the document in which the word belongs
     * @param word
     */
    private void addWord(int id, String word) {
        //To the index
        if (index.containsKey(id) == false) {
            index.put(id, new HashMap<String, Double>());
        }

        if (index.get(id).containsKey(word) == false) {
            index.get(id).put(word, 0.);
        }

        index.get(id).put(word, index.get(id).get(word) + 1);

        //To the reverse
        if (reverse.containsKey(word) == false) {
            reverse.put(word, new HashMap<Integer, Integer>());
        }

        if (reverse.get(word).containsKey(id) == false) {
            reverse.get(word).put(id, 0);
        }

        reverse.get(word).put(id, 1 + reverse.get(word).get(id));
    }
}
