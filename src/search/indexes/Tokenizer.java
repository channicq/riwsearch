package search.indexes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author clement
 */
public class Tokenizer {

    public List<String> stopWords;

    public Tokenizer() {
        stopWords = new ArrayList<>();
    }

    public static List<String> toWords(String line) {
        List<String> result = new ArrayList<>();
        String[] words = line.split("[^-a-zA-Z0-9]+");
        for (String word : words) {
            result.add(word.trim().toLowerCase());
        }

        return result;
    }

    void load(String path) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(path));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            stopWords.add(line.trim());
            line = br.readLine();
        }
        
        System.out.println("Loaded " + stopWords.size() + " stopwords from file."); 
    }
    
    public boolean isStopWord(String word){
        for(String stopWord : stopWords){
            if(stopWord.equals(word)){
                return true;
            }
        }
        return false;
    }
    
    public List<String> tokenify(String line){
        List<String> tokens = new ArrayList<>();
        List<String> words = toWords(line);
        for(String word : words){
            if(isStopWord(word) == false){
                tokens.add(word);
            }
        }
        
        return tokens;
    }
}
