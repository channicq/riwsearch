package search.indexes;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author clement
 */
public class Ponderator {

    public static final String BASE = "base";
    public static final String TFIDF = "tfidf";
    public static final String TF = "tf";


    static Map<Integer, Map<String, Double>> generate(String method, Map<Integer, Map<String, Double>> index, Map<String, Map<Integer, Integer>> reverse) throws PonderationException {
        if (method.equals(TFIDF)) {
            return generateTFIDF(index, reverse);
        } else if (method.equals(TF)){
            return generateTF(index);
        } else{
            throw new PonderationException("Ponderation not found : " + method);
        }
    }

    private static Map<Integer, Map<String, Double>> generateTFIDF(Map<Integer, Map<String, Double>> index, Map<String, Map<Integer, Integer>> reverse) {
        //Generate the TF table
        Map<Integer, Map<String, Double>> tf = generateTF(index);
        //Generates the IDF table
        Map<String, Double> idf = generateIDF(reverse, index.size());
        //Compiling results
        Map<Integer, Map<String, Double>> tfidfTable = new HashMap<>();
        for(Map.Entry<Integer, Map<String, Double>> document : tf.entrySet()){
            Map<String, Double> tfidfWords = new HashMap<>();
            for(Map.Entry<String, Double> word : document.getValue().entrySet()){
                double tfidf = word.getValue() * idf.get(word.getKey());
                tfidfWords.put(word.getKey(), tfidf);
            }
            tfidfTable.put(document.getKey(), tfidfWords);
        }
        return tfidfTable;
    }

    /**
     * Generates a pondered hashtable
     * @param index
     * @return 
     */
    private static Map<Integer, Map<String, Double>> generateTF(Map<Integer, Map<String, Double>> index) {
        Map<Integer, Map<String, Double>> results = new HashMap<>();
        for(Map.Entry<Integer, Map<String, Double>> document : index.entrySet()){
            Map<String, Double> tfDocument = new HashMap<>();
            double sum = 0;
            for(Map.Entry<String, Double> word : document.getValue().entrySet()){
                sum += word.getValue();
            }
            for(Map.Entry<String, Double> word : document.getValue().entrySet()){
                tfDocument.put(word.getKey(), word.getValue()/sum);
            }
            results.put(document.getKey(), tfDocument);
        }
        
        return results;
    }

    /**
     * Computes the idf part
     * @param reverse
     * @param D
     * @return 
     */
    private static Map<String, Double> generateIDF(Map<String, Map<Integer, Integer>> reverse, int D) {
        Map<String, Double> idfTable = new HashMap<>();
        for(Map.Entry<String, Map<Integer, Integer>> word : reverse.entrySet()){
            double idf = Math.log(D / (word.getValue().size()));
            idfTable.put(word.getKey(), idf);
        }
        
        return idfTable;
    }

}
