package search.compare;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author clement
 */
public class WordVector {

    private Map<String, Double> vector;
    private final double selfNorm;

    public WordVector(List<String> words) {
        this.vector = new HashMap<>();
        for (String word : words) {
            if (this.vector.containsKey(word) == false) {
                this.vector.put(word, 0.);
            }
            this.vector.put(word, this.vector.get(word) + 1);
        }

        this.selfNorm = norm(vector);
    }

    /**
     * Computes the norm of a given vector given on the form
     * Map<String, Integer>
     *
     * @param vector
     * @return
     */
    private static Double norm(Map<String, Double> vector) {
        double sum = 0;
        for (Map.Entry<String, Double> entry : vector.entrySet()) {
            sum += entry.getValue() * entry.getValue();
        }

        return Math.sqrt(sum);
    }

    public double product(Map<String, Double> otherVector) {
        double result = 0;
        for (Map.Entry<String, Double> entry : vector.entrySet()) {
            if (otherVector.containsKey(entry.getKey())) {
                result += entry.getValue() * otherVector.get(entry.getKey());
            }
        }
        result = result / (selfNorm * norm(otherVector));
        
        return result;
    }
}
