package search.evaluation;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import search.indexes.DB;
import search.indexes.Ponderator;
import search.model.Result;
import search.queries.Query;
import search.queries.QueryException;

public class Evaluator {

    private long startTime = 0;
    private String path;
    private HashMap<Integer, List<Integer>> relResults;
    private HashMap<Integer, Query> queries;
    private float mean = 0;
    private static final String RAPPEL = "rappel";
    private static final String PRECISION = "precision";
    
    public Evaluator(){
    	this.relResults = new HashMap<Integer, List<Integer>>();
        this.queries = new HashMap<Integer, Query>();
    }
    
    /**
     * start the timer
     */
    public void startTimer() {
        this.startTime = System.currentTimeMillis();
    }

    /**
     * end the timer 
     */
    public void endTimer() {
        System.out.println(" Time spend : " + (System.currentTimeMillis() - startTime) + "ms");
    }

    /**
     * evaluate all of the search modes
     * @param argument file path for sample queries and results sets
     * @param database
     */
    public void evaluate(String argument, DB database) {
        this.path = argument;
        loadResults();
        
        //evaluate vectorial search modes
        loadQueries(CACMQueryParser.TYPE_VECTORIAL);
        
        //evaluate basic vectorial search mode
        String filePath = argument + Ponderator.BASE + "_evaluations.csv";
        database.setPonderation(Ponderator.BASE);
        evaluateSingleMode(database, filePath);
        
        //evaluate TF-IDF search mode
        filePath = argument + Ponderator.TFIDF + "_evaluations.csv";
        database.setPonderation(Ponderator.TFIDF);
        evaluateSingleMode(database, filePath);
        
        //evaluate TF search mode
        filePath = argument + Ponderator.TF + "_evaluations.csv";
        database.setPonderation(Ponderator.TF);
        evaluateSingleMode(database, filePath);
        
        //evaluate Binary Independance Retrieval search mode
        filePath = argument + CACMQueryParser.TYPE_BIR + "_evaluations.csv";
        loadQueries(CACMQueryParser.TYPE_BIR);
        evaluateSingleMode(database, filePath);
        
        System.out.println("------- Evaluation terminated, .csv files saved in : " + argument + " -------");

        
    }
    
    /**
     * evaluate a single mode of search
     * @param database
     * @param filePath
     */
    public void evaluateSingleMode(DB database, String filePath){
    	String out = filePath;
    	try {
            Map<Integer, Map<String, Double>> results = evaluateQueries(database);
            PrintWriter pw = new PrintWriter(new FileWriter(out));
            pw.write("ID,"+PRECISION+","+RAPPEL+"\n");
            for (Map.Entry<Integer, Map<String, Double>> entry : results.entrySet()) {
                pw.write(entry.getKey() + "," +entry.getValue().get(PRECISION)
                    + "," +entry.getValue().get(RAPPEL) + "\n");
            }

            pw.close();
        } catch (QueryException e) {
        	System.out.println("Query exception");
        } catch (IOException ex) {
            System.out.println("Could not write output file: " +out);
        }
    }

    /**
     * evaluate results for one query
     * @param database
     * @return
     * @throws QueryException
     */
    public Map<Integer, Map<String, Double>> evaluateQueries(DB database) throws QueryException {
        int noResultQueries = 0;
        HashMap<Integer, Map<String, Double>> statistics = new HashMap<>();
        for (int i : queries.keySet()) {
            if (relResults.containsKey(i)) {
                double p = 0, r, m_e, m_f, m_r, mean_temp = 0;
                List<Result> results = database.execute(queries.get(i));

                for (int j = 1; j < results.size(); j++) {
                    p = precision(relResults.get(i), results, results.size());
                    mean_temp += p;
                }

                mean += mean_temp / results.size();
                r = rappel(relResults.get(i), results);
                m_e = measureE(p, r);
                m_f = measureF(m_e);
                m_r = measureR(relResults.get(i), results, p);
                System.out.println(String.format("Query %d: precision : %f, rappel : %f, Measure E : %f, Measure F : %f, Measure R : %f", i, p, r, m_e, m_f, m_r));
                statistics.put(i, new HashMap<String, Double>());
                statistics.get(i).put(PRECISION, p);
                statistics.get(i).put(RAPPEL, r);
            } else {
                //System.out.println(String.format("Query %d: no relevant result expected", i));
                noResultQueries++;
            }
        }

        //exclude queries with no relevent results expected
        mean = mean / (queries.size() - noResultQueries);
        System.out.println("------- Evaluation terminated, mean average precision : " + mean + " -------");

        return statistics;
    }

    /**
     * load sample cacm queries' relevant results set
     */
    private void loadResults() {
        EvaluationResultsParser parser = new EvaluationResultsParser(this.path + "qrels.text");
        try {
            relResults = parser.parse();
        } catch (FileNotFoundException e) {
        	System.out.println("Query files not found !");
        } catch (IOException e) {
        	System.out.println("Error when trying to open the sample query results file : " + this.path + "qrels.text");
        }

    }

    /**
     * load sample queries for cacm db
     * @param type the search mode used : BIR or vectorial
     */
    private void loadQueries(String type) {
        CACMQueryParser parser = new CACMQueryParser(this.path + "query.text");
        try {
            queries = parser.parse(type);
        } catch (FileNotFoundException e) {
        	System.out.println("Query files not found !");
        } catch (IOException e) {
        	System.out.println("Error when trying to open the sample query file : " + this.path + "query.text");
        }
    }

    private double precision(List<Integer> relResults, List<Result> res, double k) {
        double count = 0.;
        for (int i = 0; i < k; i++) {
            if (relResults.contains(res.get(i).getDocument().getId())) {
                count += 1;
            }
        }
        return count / k;
    }

    private double rappel(List<Integer> relResults, List<Result> res) {
        double count = 0.;
        for (Result r : res) {
            if (relResults.contains(r.getDocument().getId())) {
                count += 1;
            }
        }
        return count / ((double) relResults.size());
    }

    private double measureE(double p, double r) {
        return 1 - ((p / r) * (p / r) + 1) * p * r / ((p * p * p / (r * r)) + r);
    }

    private double measureF(double p, double r) {
        return ((p / r) * (p / r) + 1) * p * r / ((p * p * p / (r * r)) + r);
    }

    private double measureF(double e) {
        return 1 - e;
    }

    private double measureR(List<Integer> relResults, List<Result> res, double p) {
        return precision(relResults, res, p * res.size());
    }

}
