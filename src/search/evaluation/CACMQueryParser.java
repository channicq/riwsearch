package search.evaluation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import search.queries.BIRQuery;
import search.queries.Query;
import search.queries.VectorialQuery;

/**
 *
 * @author clement
 */

public class CACMQueryParser {

    private final Pattern regex = Pattern.compile("^\\.([A-Z]) ?(.*)$");
    private char currentFlag = 'I';
    private int currentId = 0;
	private String path;
	private HashMap<Integer, Query> queries;
	private String currentQueryString = "";
	
	public static final String TYPE_BIR = "bir";
	public static final String TYPE_VECTORIAL = "vectorial";
	

    public CACMQueryParser(String path) {
        this.path = path;
        queries = new HashMap<>();
    }
    
    
    /**
     * generate a query of the requested query type
     * @param q the main string of the query 
     * @param type the type of the query
     * @return query
     */
    private Query generate(String q, String type){
    	if(type.equals(TYPE_VECTORIAL)){
    		return new VectorialQuery(q);
    	}
    	return new BIRQuery(q);
    }
    
    /**
     * Parses the file specified in the constructor
     * @return list of documents
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public HashMap<Integer, Query> parse(String type) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(this.path));
        try {
            String line = br.readLine();

            while (line != null) {
                handle(line);
                line = br.readLine();
            }
            
            //add the last query
            this.queries.put(currentId, generate(currentQueryString,type));
            
        } finally {
            br.close();
        }
        
        return queries;
    }

    /**
     * Handles a single line
     * @param line 
     */
    void handle(String line) {
        Matcher matcher = regex.matcher(line);
        if (matcher.find()) {
            //We have a header
            char flag = matcher.group(1).charAt(0);
            String number = matcher.group(2);
            if (flag == 'I') {
                if (!this.currentQueryString.equals("")) {
                    this.queries.put(currentId, new VectorialQuery(currentQueryString));
                    currentQueryString = "";
                }
                currentId = Integer.parseInt(number);
            } else {
                currentFlag = flag;
            }
        } else {
        	if(currentFlag == 'W'){
        		currentQueryString += line;
        	} 
        }
    }
}