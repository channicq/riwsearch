package search.evaluation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EvaluationResultsParser {

	private final Pattern regex = Pattern.compile("([0-9]{2}) ([0-9]{4})");
	private String path;
	private HashMap<Integer, List<Integer>> map; 
	
	public EvaluationResultsParser(String path) {
		this.path = path;
		this.map = new HashMap<Integer, List<Integer>>();
	}
	
	/**
     * Parses the file specified in the constructor
     * @return list of documents
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public HashMap<Integer, List<Integer>> parse() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(this.path));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                handle(line);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return map;
    }

	void handle(String line) {
        Matcher matcher = regex.matcher(line);
        if (matcher.find()) {
        	int qid = Integer.parseInt(matcher.group(1));
            int did = Integer.parseInt(matcher.group(2));
            if(map.containsKey(qid)){
	        	if(!map.get(qid).contains(did)){
	        		List<Integer> temp = map.get(qid);
	        		temp.add(did);
	        		map.put(qid, temp);
	        	} 
	        } else {
	        	ArrayList<Integer> temp=new ArrayList<Integer>();
	        	temp.add(did);
	        	map.put(qid, temp);
	        	
	        }
        } 
	        
	}
}
