package search.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import search.model.CACMDocument;

/**
 *
 * @author clement
 */
public class CACMDocumentParser extends DocumentParser {

    private final Pattern regex = Pattern.compile("^\\.([A-Z]) ?(.*)$");
    private char currentFlag;

    public CACMDocumentParser(String path) {
        super(path);
    }

    /**
     * Handles a single line
     * @param line 
     */
    @Override
    void handle(String line) {
        Matcher matcher = regex.matcher(line);
        if (matcher.find()) {
            //We have a header
            char flag = matcher.group(1).charAt(0);
            String number = matcher.group(2);
            if (flag == 'I') {
                if (this.currentDocument != null) {
                    this.documents.add(this.currentDocument);
                }
                this.currentDocument = new CACMDocument(Integer.parseInt(number));
            } else {
                currentFlag = flag;
            }
        } else {
            ((CACMDocument) this.currentDocument).addContent(currentFlag, line);
        }
    }
}