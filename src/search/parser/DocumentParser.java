package search.parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import search.model.Document;

public abstract class DocumentParser {

    private final String path;
    List<Document> documents;
    Document currentDocument;
    int actualId;

    /**
     * Constructor opens the file and throws the error if the file isn't found
     *
     * @param path path to the file
     */
    public DocumentParser(String path) {
        this.path = path;
        this.documents = new ArrayList<>();
    }

    /**
     * Parses the file specified in the constructor
     * @return list of documents
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public List<Document> parse() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(this.path));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                handle(line);
                line = br.readLine();
            }
            //Adding current document if not null
            if(currentDocument != null){
                documents.add(currentDocument);
            }
        } finally {
            br.close();
        }
        
        return documents;
    }
    
    /**
     * Private method used to handle each line of the file.
     * @param line 
     */
    abstract void handle(String line); 
}
