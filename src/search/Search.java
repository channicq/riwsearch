package search;

import search.evaluation.Evaluator;
import search.indexes.DB;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import search.model.Document;
import search.model.Result;
import search.parser.CACMDocumentParser;
import search.queries.BIRQuery;
import search.queries.BinaryQuery;
import search.queries.Query;
import search.queries.QueryException;
import search.queries.VectorialQuery;
import search.queries.WordQuery;
import search.queries.binary.IllegalExpressionException;

/**
 *
 * @author clement
 */
public class Search {

    private static final Pattern regex = Pattern.compile("^([-_a-zA-Z]+) ?(.*)$");

    /**
     * Main while loop : command-line interface
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s;
        DB database = new DB();
        Evaluator evaluator = new Evaluator();

        if (args.length >= 1) {
            database.loadStopWords(args[0]);
        }

        //checking if auto-importing documents 
        if (args.length >= 3) {
            if (args[1].equals("--cacm")) {
                loadCacm(args[2], database);
            }
        }

        boolean cont = true;
        while (cont) {
            try {
                System.out.print("> ");
                s = in.readLine();
                Matcher matcher = regex.matcher(s);
                if (matcher.find()) {
                    String command = matcher.group(1);
                    String argument = matcher.group(2);
                    evaluator.startTimer();
                    if ("quit".equals(command)) {
                        cont = false;
                    }
                    if ("load_cacm".equals(command)) {
                        loadCacm(argument, database);
                    }
                    if("set_minimal_score".equals(command)){
                        database.setMinimalScore(Double.parseDouble(argument));
                        System.out.println("Changed minimal score to " + argument);
                    }
                    if("set_max_results".equals(command)){
                        database.setMaxResults(Integer.parseInt(argument));
                        System.out.println("Changed max quantity of results to " + (argument.equals("0")?"infinity":argument));
                    }
                    if ("document".equals(command)) {
                        int doc = Integer.parseInt(argument);
                        Document document = database.getDocument(doc);
                        if (document != null) {
                            document.print();
                        } else {
                            System.out.println("Error : document not found");
                        }
                    }
                    if ("find_word".equals(command)) {
                        find(new WordQuery(argument), database);
                    }
                    if ("find_vectorial".equals(command)) {
                        find(new VectorialQuery(argument), database);
                    }
                    if ("find_bir".equals(command)) {
                        find(new BIRQuery(argument), database);
                    }
                    if ("find_binary".equals(command)) {
                        try {
                            find(new BinaryQuery(argument), database);
                        } catch (IllegalExpressionException ex) {
                            System.out.println("Error : " + ex.getMessage());
                        }
                    }
                    if ("set_ponderation".equals(command)) {
                        database.setPonderation(argument);
                    }
                    if ("load_stopwords".equals(command)) {
                        database.loadStopWords(argument);
                    }
                    if ("evaluation".equals(command)) {
                    	//evaluation address of the folder containing the query files
                    	evaluator.evaluate(argument,database); 
                    }
                    evaluator.endTimer();
                } else {
                    System.out.println("Unrecognized input form");
                }
            } catch (IOException ex) {
                System.out.println("Error : Input console not working properly.");
            }
        }
    }

    /**
     * Runs the given query, then order and prints the results
     *
     * @param q
     */
    private static void find(Query q, DB database) {
        try {
            List<Result> results = database.execute(q);
            System.out.println("Found " + results.size() + " results");
            
            
            for (Result result : results) {
                result.print();
            }
        } catch (QueryException ex) {
            System.out.println("Error : " + ex.getMessage());
        }
    }

    /**
     * Loads a specified CACM file
     *
     * @param path to the CACM file
     * @param database database in which store the indexes
     */
    private static void loadCacm(String path, DB database) {
        CACMDocumentParser parser = new CACMDocumentParser(path);
        try {
            System.out.println("Parsing " + path + " as a CACM file, Please wait.");
            List<Document> documents = parser.parse();
            for (Document document : documents) {
                database.addDocument(document);
            }
            System.out.println("Loaded " + documents.size() + " documents.");
        } catch (FileNotFoundException e) {
            System.out.println("Error : file " + path + " not found.");
        } catch (IOException ex) {
            System.out.println("Error while reading file.");
        }
    }
}
