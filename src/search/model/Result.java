package search.model;

/**
 *
 * @author clement
 */
public class Result {

    private final double score;
    private final Document document;

    public Result(Document document, double score) {
        this.document = document;
        this.score = score;
    }

    public Double getScore() {
        return this.score;
    }

    public Document getDocument() {
        return this.document;
    }

    public void print() {
        System.out.println("[" + document.getId() + "] " + document.getTitle() + " (" + score + ")");
    }
}
