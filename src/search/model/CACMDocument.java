/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package search.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import search.indexes.Tokenizer;

/**
 *
 * @author clement
 */
public class CACMDocument extends Document{
    private final Map<Character, List<String>> contents;
    
    /**
     * Needing the Id as an Integer
     * @param id 
     */
    public CACMDocument(int id){
        this.id = id;
        this.contents = new HashMap<>();
    }
    
    public void addContent(char flag, String line){
        if(contents.containsKey(flag) == false)
            contents.put(flag, new ArrayList<String>());
        contents.get(flag).add(line);
    }
    
    /**
     * Prints the document in stdout
     */
    @Override
    public void print(){
        for(Map.Entry<Character, List<String>> entry : contents.entrySet()){
            System.out.println(" -- " + entry.getKey());
            for(String line : entry.getValue()){
                System.out.println(line);
            }
        }
    }
    
    /**
     * Gets all the words in the pertinent fields (T, W and K)
     * @return List of words
     */
    @Override
    public List<String> getWords(){
        return getWords("TWK");
    }
    
    /**
     * Gets all the words in the requested fields
     * @param flags String containing each flag (as a character);
     * @return 
     */
    public List<String> getWords(String flags){
        List<String> words = new ArrayList<>();
        for(char flag : flags.toCharArray()){
            if(contents.containsKey(flag))
            for(String line : contents.get(flag)){
                List<String> tokens = Tokenizer.toWords(line);
                for(String word : tokens){
                    words.add(word);
                }
            }
        }
        
        return words;
    }

    @Override
    public String getTitle() {
        if(contents.containsKey('T'))
            return contents.get('T').get(0);
        else
            return "--Unnamed Document--";
    }
}
