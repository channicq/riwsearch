package search.model;

import java.util.List;

public abstract class Document {
    int id;
    
    public int getId(){
        return id;
    }
    
    /**
     * Prints the document in sout
     */
    public abstract void print();
    
    /**
     * Gets all the words in the pertinent fields
     * @return List of words
     */
    public abstract List<String> getWords();
    
    /**
     * Gets the title of the document for Result display
     * @return 
     */
    public abstract String getTitle();
}
